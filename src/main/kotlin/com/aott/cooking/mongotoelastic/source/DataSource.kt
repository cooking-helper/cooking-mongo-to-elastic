package com.aott.cooking.mongotoelastic.source

import com.aott.cooking.mongotoelastic.model.synchronize.SynchronizationOperation

abstract class DataSource {
    abstract fun getAllObjectsToSync(): Map<String, Long>
    abstract fun getOneObject(objectIdToAdd: String): Map<String, Any>
    abstract fun getNextOperation(operation: Any): SynchronizationOperation
    abstract fun getLastRecoveryDate(): Long
    abstract fun getOperationIterator(lastRecoveryDate: Long): Iterator<Any>
}