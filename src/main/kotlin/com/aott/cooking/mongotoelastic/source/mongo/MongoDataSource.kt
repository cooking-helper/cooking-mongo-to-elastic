package com.aott.cooking.mongotoelastic.source.mongo

import com.aott.cooking.importer.model.Recipe
import com.aott.cooking.mongotoelastic.model.synchronize.SynchronizationOperation
import com.aott.cooking.mongotoelastic.source.DataSource
import com.aott.cooking.mongotoelastic.source.mongo.model.Oplog
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.mongodb.CursorType
import com.mongodb.client.FindIterable
import com.mongodb.client.MongoCursor
import org.bson.BsonTimestamp
import org.bson.types.ObjectId
import org.litote.kmongo.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository


@Repository
class MongoDataSource @Autowired constructor(
        private val mongo: MongoConnection
) : DataSource() {
    @Throws(Exception::class)
    override fun getNextOperation(operation: Any): SynchronizationOperation {
        if (operation is Oplog) {
            return operation.toSynchronizationOperation()
        } else {
            throw Exception("Unsupported operation type")
        }
    }

    override fun getOperationIterator(lastRecoveryDate: Long): MongoCursor<Oplog> {
        val lastRecoveryDateAsBson = BsonTimestamp(lastRecoveryDate)
        val query = "{ns:\"cooking.recipe\", ts: {\$gt:Timestamp(${lastRecoveryDateAsBson.time},${lastRecoveryDateAsBson.inc})}}"
        return mongo.localDatabase.getCollection<Oplog>("oplog.rs").find(query).cursorType(CursorType.TailableAwait).noCursorTimeout(true).oplogReplay(true).iterator()
    }

    override fun getOneObject(objectIdToAdd: String): Map<String, Any> {
        val recipeToFind = mongo.recipeDatabase.getCollection<Recipe>().findOne(Recipe::id eq ObjectId(objectIdToAdd))
        val jacksonMapper = ObjectMapper()
        return jacksonMapper.convertValue(recipeToFind, object :
                TypeReference<Map<String, Any>>() {})
    }

    override fun getAllObjectsToSync(): Map<String, Long> {
        val recipesIdAndUpdateDate = mutableMapOf<String, Long>()
        val recipes: FindIterable<Recipe> = mongo.recipeDatabase.getCollection<Recipe>().find()
        for (recipe in recipes) {
            recipesIdAndUpdateDate[recipe.id.toString()] = recipe.updateDate.time
        }
        return recipesIdAndUpdateDate
    }

    @Throws(Exception::class)
    override fun getLastRecoveryDate(): Long {
        val result: FindIterable<Oplog> = mongo.localDatabase.getCollection<Oplog>("oplog.rs").find("{ns:\"cooking.recipe\"}").sort("{ts:-1}").limit(1)
        val lastOplog = result.first();
        if (lastOplog != null) {
            return lastOplog.ts.value
        } else {
            throw Exception("Nothing on the oplogs for this collection")
        }
    }
}