package com.aott.cooking.mongotoelastic.source.mongo.model

import com.aott.cooking.mongotoelastic.model.synchronize.SynchronizationOperation
import com.aott.cooking.mongotoelastic.model.synchronize.SynchronizationOperationType
import org.bson.BsonTimestamp

data class Oplog(val ts: BsonTimestamp, val op: String, val o: Map<String, Any>) {
    fun toSynchronizationOperation(): SynchronizationOperation {
        val id: String = o.get("_id").toString()
        val operation = getOperationType(op)
        val objectProperties = o
        return SynchronizationOperation(operation, id, objectProperties)
    }

    private fun getOperationType(op: String): SynchronizationOperationType {
        when {
            op.equals("i") -> return SynchronizationOperationType.INSERT
            op.equals("u") -> return SynchronizationOperationType.UPDATE
            op.equals("d") -> return SynchronizationOperationType.DELETE
            else -> {
                throw Exception("Unknown operation type")
            }
        }
    }
}
