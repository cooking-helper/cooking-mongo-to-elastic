package com.aott.cooking.mongotoelastic.controller

import com.aott.cooking.mongotoelastic.model.synchronize.SynchronizationOperation
import com.aott.cooking.mongotoelastic.model.synchronize.SynchronizationOperationType
import com.aott.cooking.mongotoelastic.source.mongo.MongoDataSource
import com.aott.cooking.mongotoelastic.target.elastic.ElasticDataTarget
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller

@Controller
class SynchronizationController @Autowired constructor(private val source: MongoDataSource, @Autowired private val target: ElasticDataTarget) {
    fun startSynchronization() {
        val lastRecoveryDate: Long = source.getLastRecoveryDate()
        synchronizeAll()
        synchronizeOnTheFly(lastRecoveryDate)
    }

    private fun synchronizeOnTheFly(lastRecoveryDate: Long) {
        val iterator: Iterator<Any> = source.getOperationIterator(lastRecoveryDate)
        while (iterator.hasNext()) {
            val sourceEvent: SynchronizationOperation = source.getNextOperation(iterator.next())
            when (sourceEvent.type) {
                SynchronizationOperationType.INSERT -> target.insertObject(sourceEvent.objectId, sourceEvent.objectProperties)
                SynchronizationOperationType.DELETE -> target.deleteObject(sourceEvent.objectId)
                SynchronizationOperationType.UPDATE -> target.updateObject(sourceEvent.objectId, sourceEvent.objectProperties)
            }
        }
    }

    private fun synchronizeAll() {
        val sourceObjects: Map<String, Long> = source.getAllObjectsToSync()
        val targetObjects: Map<String, Long> = target.getAllObjectsToSync()

        val objectIdsToAdd: Set<String> = sourceObjects.keys.minus(targetObjects.keys)

        for (objectIdToAdd in objectIdsToAdd) {
            val sourceObject: Map<String, Any> = source.getOneObject(objectIdToAdd)
            target.insertObject(objectIdToAdd, sourceObject)
        }


        val objectIdsToDelete: Set<String> = targetObjects.keys.minus(sourceObjects.keys)

        for (objectIdToDelete in objectIdsToDelete) {
            target.deleteObject(objectIdToDelete)
        }

        val objectIdsToCheckForUpdate: Set<String> = targetObjects.keys.intersect(sourceObjects.keys)

        val objectIdsToUpdate = objectIdsToCheckForUpdate.stream().filter { objectIdToCheckForUpdate ->
            isObjectToUpdate(objectIdToCheckForUpdate, sourceObjects, targetObjects)
        }

        for (objectIdToUpdate in objectIdsToUpdate) {
            val sourceObject: Map<String, Any> = source.getOneObject(objectIdToUpdate)
            target.updateObject(objectIdToUpdate, sourceObject)
        }
    }

    private fun isObjectToUpdate(objectIdToCheckForUpdate: String, sourceObjects: Map<String, Long>, targetObjects: Map<String, Long>): Boolean {
        return sourceObjects[objectIdToCheckForUpdate] != targetObjects[objectIdToCheckForUpdate]
    }
}