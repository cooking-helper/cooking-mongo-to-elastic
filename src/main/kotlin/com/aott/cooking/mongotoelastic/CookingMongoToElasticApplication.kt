package com.aott.cooking.mongotoelastic

import com.aott.cooking.mongotoelastic.controller.SynchronizationController
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
import org.springframework.boot.runApplication
import org.springframework.context.ConfigurableApplicationContext

@SpringBootApplication(exclude = [MongoAutoConfiguration::class, MongoDataAutoConfiguration::class])
class CookingMongoToElasticApplication

fun main(args: Array<String>) {
    val context: ConfigurableApplicationContext = runApplication<CookingMongoToElasticApplication>(*args)
    context.getBean(SynchronizationController::class.java).startSynchronization()
}



