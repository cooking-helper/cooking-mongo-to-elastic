package com.aott.cooking.mongotoelastic.model.synchronize

enum class SynchronizationOperationType {
    INSERT,
    UPDATE,
    DELETE
}