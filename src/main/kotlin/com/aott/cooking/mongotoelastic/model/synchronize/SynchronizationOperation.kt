package com.aott.cooking.mongotoelastic.model.synchronize

data class SynchronizationOperation(val type: SynchronizationOperationType, val objectId: String, val objectProperties: Map<String, Any>)