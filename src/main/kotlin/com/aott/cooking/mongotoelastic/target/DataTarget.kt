package com.aott.cooking.mongotoelastic.target

abstract class DataTarget {
    abstract fun getAllObjectsToSync(): Map<String, Long>
    abstract fun insertObject(sourceObjectId: String, sourceObject: Map<String, Any>)
    abstract fun deleteObject(objectIdToDelete: String)
    abstract fun updateObject(sourceObjectId: String, sourceObject: Map<String, Any>)
}
