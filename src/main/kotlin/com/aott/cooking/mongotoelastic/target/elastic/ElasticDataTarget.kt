package com.aott.cooking.mongotoelastic.target.elastic

import com.aott.cooking.mongotoelastic.target.DataTarget
import org.elasticsearch.action.ActionListener
import org.elasticsearch.action.DocWriteResponse
import org.elasticsearch.action.delete.DeleteRequest
import org.elasticsearch.action.delete.DeleteResponse
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.action.index.IndexResponse
import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.action.search.SearchScrollRequest
import org.elasticsearch.common.unit.TimeValue
import org.elasticsearch.search.Scroll
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import java.util.*


@Repository
class ElasticDataTarget @Autowired constructor(
        private val elastic: ElasticConnection
) : DataTarget() {
    val INDEX = "cooking"
    val TYPE = "recipe"
    override fun updateObject(sourceObjectId: String, sourceObject: Map<String, Any>) {
        insertOrUpdate(sourceObjectId, sourceObject)
    }

    override fun insertObject(sourceObjectId: String, sourceObject: Map<String, Any>) {
        insertOrUpdate(sourceObjectId, sourceObject)
    }

    fun insertOrUpdate(sourceObjectId: String, sourceObject: Map<String, Any>) {
        val elasticObject = convertForElastic(sourceObject)
        val insertRequest = IndexRequest(INDEX, TYPE, sourceObjectId)
                .source(elasticObject)
        elastic.client.indexAsync(insertRequest, getInsertOrUpdateListener())
    }

    private fun getInsertOrUpdateListener(): ActionListener<IndexResponse>? {
        return object : ActionListener<IndexResponse> {
            override fun onResponse(indexResponse: IndexResponse) {
                if (indexResponse.result == DocWriteResponse.Result.CREATED) {
                    System.out.println("Doc inserted with id: " + indexResponse.id)
                } else if (indexResponse.result == DocWriteResponse.Result.UPDATED) {
                    System.out.println("Doc updated with id: " + indexResponse.id)
                }
            }

            override fun onFailure(e: Exception) {
                System.out.println("Error inserting doc because " + e.message)
            }
        }
    }

    private fun convertForElastic(sourceObject: Map<String, Any>): Map<String, Any> {
        return sourceObject.filter(removeId()).mapValues(convertDataType())
    }

    private fun convertDataType(): (Map.Entry<String, Any>) -> Any {
        return { entry ->
            when (entry.value) {
                is Date -> (entry.value as Date).time
                else -> {
                    entry.value
                }
            }
        }
    }

    private fun removeId(): (Map.Entry<String, Any>) -> Boolean = { entry -> entry.key != "id" && entry.key != "_id" }

    override fun deleteObject(objectIdToDelete: String) {
        val request = DeleteRequest(
                INDEX,
                TYPE,
                objectIdToDelete)


        elastic.client.deleteAsync(request, getDeleteListener());
    }

    private fun getDeleteListener(): ActionListener<DeleteResponse>? {
        return object : ActionListener<DeleteResponse> {
            override fun onResponse(deleteResponse: DeleteResponse) {
                System.out.println("Doc deleted with id: " + deleteResponse.id)
            }

            override fun onFailure(e: Exception) {
                System.out.println("Error deleting doc because " + e.message)
            }
        }
    }

    override fun getAllObjectsToSync(): Map<String, Long> {
        val formattedResults = mutableMapOf<String, Long>()

        val scroll = Scroll(TimeValue.timeValueMinutes(1L))

        val searchRequest = SearchRequest(INDEX)
        searchRequest.scroll(scroll);
        val searchSourceBuilder = SearchSourceBuilder()
        searchSourceBuilder.size(1000)
        searchRequest.source(searchSourceBuilder)

        var searchScrollResponse = elastic.client.search(searchRequest)
        var scrollId = searchScrollResponse.scrollId
        var searchHits = searchScrollResponse.hits.hits

        while (searchHits != null && searchHits.isNotEmpty()) {
            for (hit in searchHits) {
                val entry = hit.sourceAsMap
                formattedResults[hit.id] = entry["updateDate"] as Long
            }

            val scrollRequest = SearchScrollRequest(scrollId)
            scrollRequest.scroll(scroll)
            searchScrollResponse = elastic.client.searchScroll(scrollRequest)
            scrollId = searchScrollResponse.scrollId
            searchHits = searchScrollResponse.hits.hits
        }
        return formattedResults
    }
}
