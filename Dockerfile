FROM openjdk:8 AS build_image
ENV APP_HOME=/root/dev/cooking-mongo-to-elastic/
RUN mkdir -p $APP_HOME/src/main/java
WORKDIR $APP_HOME
COPY build.gradle gradlew gradlew.bat $APP_HOME
COPY gradle $APP_HOME/gradle
# download dependencies
RUN chmod 777 gradlew
RUN ./gradlew build -x test || return 0
COPY . .
RUN ./gradlew build -x test


FROM openjdk:8-jre
WORKDIR /root/
COPY --from=build_image /root/dev/cooking-mongo-to-elastic/build/libs/cooking-mongo-to-elastic*.jar ./cooking-mongo-to-elastic.jar
COPY src/main/resources/docker_config.properties /root/config.properties
EXPOSE 8080
CMD ["java","-jar","cooking-mongo-to-elastic.jar","--spring.config.location=file:///root/config.properties"]